# Adapted from
# https://jtway.co/dockerfile-for-a-crystal-application-1e9db24efbc2
# Step 1: Build image
FROM docker.io/crystallang/crystal:1.8.2-alpine as builder
WORKDIR /app
# Cache dependencies
COPY ["./shard.yml", "./shard.lock", "/app/"]
RUN shards install --production -v
# Build a binary
COPY src /app/src
RUN shards build --static --no-debug --release --production -v
# ===============

# tag::production[]
# Step 2: Runtime image
FROM docker.io/library/alpine:3.17
RUN apk add --no-cache fortune
WORKDIR /
COPY --from=builder /app/bin/fortune_crystal .

EXPOSE 8080

# <1>
USER 1001:0

ENTRYPOINT ["/fortune_crystal"]
# end::production[]
