require "kemal"

version = "1.2-crystal"
hostname = `hostname`

# tag::router[]
get "/" do |env|
  message = `fortune`
  number = rand(1000)
  accept = env.request.headers["Accept"]
  if accept == "application/json"
    obj = { :version => version,
            :hostname => hostname,
            :message => message,
            :number => number }
    obj.to_json
  elsif accept == "text/plain"
    "Fortune %s cookie of the day #%d:\n\n%s" % [version, number, message]
  else
    render "src/views/fortune.ecr"
  end
end
# end::router[]

Kemal.config.port = 8080
Kemal.run
